
import java.util.*;

/**
 * 
 */
public class Partida {

    /**
     * Default constructor
     */
    public Partida() {
    }

    /**
     * 
     */
    public int id;

    /**
     * 
     */
    public String equipo1;

    /**
     * 
     */
    public String equipo2;

    /**
     * 
     */
    public String torneo;

    /**
     * 
     */
    public String fecha;

    /**
     * 
     */
    public String resultado;

}